<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default_hello")
     *
     * @return Responser
     */
    public function helloWorld()
    {
        return $this->render('default/hello-world.html.twig');
    }
}